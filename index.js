var reload_timeout = 0;
function refreshStatus(c, e, b) {
    if (c == null) {
        return handleError(b, e, null)
    }
    var a = (c.wireless.mode == "ap");
    setOperationMode(c);
    update_basic(c);
    update_cpu_usage(c);
    update_mem_usage(c);
    update_polling(a, c.wireless.polling, c.wireless);
    update_misc(c);
    update_ifinfo(c);
    update_antenna(c.wireless.antenna);
    update_chains(c.wireless.chains);
    update_unms(c);
    if (global.has_gps) {
        update_gps(c.gps)
    }
    if (document.getElementById("airgw_info")) {
        update_airgw(c.airgw)
    }
}
function handleError(b, c, a) {
    if (b && b.status != 200 && b.status != 0) {
        window.location.reload()
    }
}
function reloadStatus() {
    $.ajax({
        url: "status.cgi",
        cache: false,
        dataType: "json",
        success: refreshStatus,
        error: handleError,
        complete: function(b, a) {
            if (reload_timeout) {
                clearTimeout(reload_timeout)
            }
            reload_timeout = setTimeout(reloadStatus, 1000)
        }
    });
    return false
}
function setOperationMode(o) {
    var d = o.wireless;
    var e = getIface(o, "ath0");
    var m = o.airview;
    var l = o.host.netrole;
    var b = o.services;
    var j = o.firewall;
    var c = (d.mode == "ap");
    var r = (e && e.enabled) ? getModeString(c, d.wds, d.aprepeater) : jsTranslate("Disabled");
    var n;
    if (m.enabled == 1) {
        old_mode_string = r;
        r = jsTranslate("Spectrum Analyzer");
        if (m.status.active_connections > 0) {
            n = jsTranslate("Active") + " - " + m.status.active_connections + " " + jsTranslate("clients")
        } else {
            if (m.watchdog.seconds_to_exit > 0) {
                n = jsTranslate("Idle for") + " " + m.watchdog.seconds_idle + jsTranslate("s") + ".  " + jsTranslate("Back to") + " " + old_mode_string + " " + jsTranslate("in") + " " + m.watchdog.seconds_to_exit + jsTranslate("s")
            } else {
                n = jsTranslate("Switching back to") + " " + old_mode_string
            }
        }
    } else {
        n = ""
    }
    var a = false
      , t = false;
    switch (l) {
    case "bridge":
        netmodestr = jsTranslate("Bridge");
        t = true;
        break;
    case "router":
        netmodestr = jsTranslate("Router");
        break;
    case "soho":
        netmodestr = jsTranslate("SOHO Router");
        a = true;
        break
    }
    $("#netmode").text(netmodestr);
    $("#wmode").text(r);
    $("#astatus").text(n);
    $("#astatusinfo").toggle(n != "");
    $(".apinfo").toggle(c);
    $(".stainfo").toggle(!c);
    $(".bridge").toggle(t);
    $(".router").toggle(!t);
    $("#wanmac").toggle(a);
    $("#dhcpc_info").toggle(1 == b.dhcpc);
    $("#dhcp_leases").toggle(1 == b.dhcpd);
    $("#ppp_info").toggle(1 == b.pppoe);
    $("#a_stainfo").attr("href", "stainfo.cgi?ifname=" + global.wlan_iface + "&sta_mac=" + d.apmac + "&mode=ap");
    $("#a_fw").attr("href", "fw.cgi?netmode=" + l);
    var f = (d.chanbw == 0) ? d.chwidth : d.chanbw;
    var g = parseFloat(d.frequency);
    var k = parseFloat(f) / 2;
    if (d.cwmmode == 1) {
        var h = "";
        if (global.has_ht40) {
            h = jsTranslate("Auto") + " "
        }
        if ((d.rstatus == 1) && (d.chanbw == 0)) {
            h += "20 / 40";
            k = 40 / 2
        } else {
            h += f
        }
        f = h
    }
    if (e.enabled) {
        if (!isRadarEnabled(d)) {
            $("#wd").text(f + " MHz");
            var p = setExtendedChannel(d.opmode);
            if (p == 1) {
                $("#freqstart").text(g - k / 2);
                $("#freqstop").text(g + k + k / 2 + " MHz")
            } else {
                if (p == -1) {
                    $("#freqstart").text(g - k - k / 2);
                    $("#freqstop").text(g + k / 2 + " MHz")
                } else {
                    $("#freqstart").text(g - k);
                    $("#freqstop").text(g + k + " MHz")
                }
            }
        } else {
            $("#wd").text("-");
            $("#freqstart").text("-");
            $("#freqstop").text("-")
        }
    } else {
        $("#wd").text("-");
        $("#ext_chan").hide()
    }
    $("#freqwidthinfo").toggle(e.enabled);
    var q = !t ? j.iptables || j.ip6tables : j.ebtables || j.eb6tables;
    $("#fwall").toggle(q == 1);
    $("#nolinfo").toggle((d.nol_chans !== undefined) && (d.nol_chans == 1));
    $(".gpsinfo").toggle(global.has_gps && o.gps.status != 0)
}
function refreshContent(a, b) {
    autoLogout.start(global.timeout);
    reloadStatus();
    if (a.indexOf("?") > 0) {
        a = a + "&id="
    } else {
        a = a + "?id="
    }
    $("#extraFrame").load(a + (new Date().getTime()), b, function(d, c, e) {
        if (e && e.status != 200 && e.status != 0) {
            window.location.reload();
            return
        }
        $.ready()
    });
    return false
}
function format_rate(a) {
    return parseInt(a) > 0 ? "" + a + " Mbps" : "-"
}
function format_ccq(a) {
    if (!a) {
        return "-"
    }
    var b = (a % 10 != 0) ? 1 : 0;
    return "" + toFixed(a / 10, b) + " %"
}
function strip_fwversion(c, d) {
    var e = "";
    if (d) {
        if ((d == "XW") || (d == "XM") || (d == "TI")) {
            e = " (" + d + ")"
        }
    }
    if (c.indexOf("-") > 0) {
        var a = c.split(".");
        if (a.length > 3) {
            var b = "";
            for (i = 0; i < a.length - 3; i++) {
                b += a[i] + "."
            }
            b += a[i];
            return b + e
        }
    }
    return c + e
}
function format_freq(a) {
    if (a.dfs == 0) {
        return "" + a.frequency
    }
    if (a.cac_nol & 2) {
        return "" + a.frequency + " (DFS Radar)"
    }
    if (a.cac_nol & 1) {
        return "" + a.frequency + " (DFS Wait)"
    }
    return "" + a.frequency + " (DFS)"
}
function format_atpc(a) {
    switch (a.polling.atpc_status) {
    case 1:
        return " (Adjusting)";
    case 2:
        return " (Auto)";
    case 3:
        return " (Auto failed)";
    case 4:
        return " (Auto, limits reached)";
    default:
        return ""
    }
}
function format_txpower(a, b) {
    if (a.txpower !== "") {
        return a.txpower + " dBm" + format_atpc(a)
    }
    return b.enabled ? "0 dBm" : "-"
}
function update_basic(k) {
    var j = k.host;
    var c = k.wireless;
    var h = getIface(k, "ath0");
    $("#signalinfo .switchable").toggle(c.rstatus == 5);
    updateSignalLevel(c.signal, c.rssi, c.noisef, c.rx_chainmask, c.chainrssi, c.chainrssiext);
    if ($("#hostname").text() != j.hostname) {
        $("#hostname").text(_uesc(j.hostname))
    }
    if ($("#devmodel").text() != j.devmodel) {
        $("#devmodel").text(j.devmodel)
    }
    var d = strip_fwversion(j.fwversion, j.fwprefix);
    if ($("#fwversion").text() != d) {
        $("#fwversion").text(d)
    }
    if (c.mode == "ap" && c.hide_essid == 1) {
        $("#essid_label").text(jsTranslate("Hidden SSID"))
    }
    var b = (c.apmac != "00:00:00:00:00:00") && (c.mode == "ap" || c.rstatus == 5);
    if (b) {
        if ($("#apmac").text() != c.apmac) {
            $("#apmac").text(c.apmac)
        }
    } else {
        $("#apmac").text(jsTranslate("Not Associated"))
    }
    if (h.enabled) {
        if (c.countrycode == 902) {
            var g = "" + c.frequency;
            var f = "" + c.channel;
            switch (c.frequency) {
            case "905 MHz":
                g = "904.75";
                f = "1";
                break;
            case "918 MHz":
                g = "918.25";
                f = "2";
                break;
            case "925 MHz":
                g = "924.75";
                f = "3";
                break;
            case "922 MHz":
                g = "921.75";
                f = "4";
                break
            }
            c.frequency = g;
            c.channel = f
        }
        if ($("#essid").text() != c.essid) {
            $("#essid").text(_uesc(c.essid))
        }
        var a = format_freq(c);
        if (!isRadarEnabled(c)) {
            if ($("#frequency").text() != a) {
                $("#frequency").text(a)
            }
            if ($("#channel").text() != c.channel) {
                $("#channel").text(c.channel)
            }
        } else {
            $("#frequency").text("-");
            $("#channel").text("-")
        }
        var e = format_rate(c.txrate);
        if ($("#txrate").text() != e) {
            $("#txrate").text(e)
        }
        e = format_rate(c.rxrate);
        if ($("#rxrate").text() != e) {
            $("#rxrate").text(e)
        }
        if ($("#count").text() != " " + c.count) {
            $("#count").text(" " + c.count)
        }
        if ($("#txpower").text() != format_txpower(c, h)) {
            $("#txpower").text(format_txpower(c, h))
        }
    } else {
        $("#frequency").text("-");
        $("#essid").text("-");
        $("#channel").text("-");
        $("#txrate").text("-");
        $("#rxrate").text("-");
        $("#count").text("-");
        $("#txpower").text("-")
    }
    $("#ack").text(update_ack(k, h))
}
var prev_cpu_total = 0;
var prev_cpu_busy = 0;
var prev_cpu_usage = 0;
var prev_cpu_uptime = 0;
function update_cpu_usage(b) {
    var c = prev_cpu_usage;
    if (b.host.cputotal != 0 && b.host.cputotal != prev_cpu_total) {
        var a = Math.round((100 * (b.host.cpubusy - prev_cpu_busy)) / (b.host.cputotal - prev_cpu_total))
    } else {
        var a = prev_cpu_usage
    }
    prev_cpu_total = b.host.cputotal;
    prev_cpu_busy = b.host.cpubusy;
    if (a < 0) {
        a = 0
    } else {
        if (a > 100) {
            a = 100
        }
    }
    if ((prev_cpu_usage != a) && (b.host.uptime - prev_cpu_uptime >= 2)) {
        prev_cpu_uptime = b.host.uptime;
        c = a
    }
    prev_cpu_usage = c;
    if (b.host.cpuload && b.host.cpuload >= 0 && b.host.cpuload <= 100) {
        c = Math.round(b.host.cpuload)
    }
    $("#cpu").text(c);
    update_meter("cpubar", c, 100)
}
function update_mem_usage(a) {
    var b = Math.round((a.host.totalram - a.host.freeram) / a.host.totalram * 100);
    if (b < 0) {
        b = 0
    } else {
        if (b > 100) {
            b = 100
        }
    }
    $("#memory").text(b);
    update_meter("membar", b, 100)
}
function update_polling(d, c, a) {
    if (c.enabled > 0) {
        $("#polling").text(jsTranslate("Enabled"));
        var b = global.chain_count;
        if (parseInt(b) == 1 && c.capacity <= 50) {
            $("#amcborder").addClass("halfborder")
        }
        $(".pollinfo").show();
        $(".stapollinfo").toggle(!d);
        $("#airselectinfo").toggle(d);
        if (c.airselect > 0) {
            $("#airselect").text(jsTranslate("Enabled"));
            $(".airselectinfo").show();
            $("#airselectinterval").text(c.airselect_interval + " ms")
        } else {
            $("#airselect").text(jsTranslate("Disabled"));
            $(".airselectinfo").hide()
        }
    } else {
        $("#polling").text(d ? jsTranslate("Disabled") : "-");
        $(".pollinfo").hide();
        $(".stapollinfo").hide();
        $("#airselectinfo").hide()
    }
    if (a.apmac == "00:00:00:00:00:00" || 0 == a.count) {
        c.quality = 0;
        c.capacity = 0
    }
    $("#pollprio").text(prio2text(c.priority));
    $("#amq").text(c.quality);
    $("#amc").text(c.capacity);
    update_meter("amqbar", c.quality, 100);
    update_meter("amcbar", c.capacity, 100);
    if (c.enabled > 0 && d && global.has_gps) {
        switch (c.airsync_mode) {
        case 1:
            $("#airsyncstatus").text(c.airsync_connections + " " + jsTranslate("Peer(s)"));
            break;
        case 2:
            if (c.airsync_connections) {
                $("#airsyncstatus").text(jsTranslate("Connected"))
            } else {
                $("#airsyncstatus").text(jsTranslate("Not Connected"))
            }
            break;
        case 0:
        default:
            $("#airsyncstatus").text(jsTranslate("Disabled"));
            break
        }
        $("#airsyncinfo").show()
    } else {
        $("#airsyncinfo").hide()
    }
}
function translate_security(b) {
    var a = global.security;
    if (a.length == 0) {
        a = b
    }
    return jsTranslate(a)
}
function update_misc(b) {
    var c = getIface(b, "ath0");
    $("#security").text(c.enabled ? translate_security(b.wireless.security) : "-");
    $("#qos").text(b.wireless.qos);
    var a = secsToCountdown(b.host.uptime, jsTranslate("day"), jsTranslate("days"));
    $("#uptime").text(a);
    $("#ccq").text(format_ccq(b.wireless.ccq));
    $("#date").text(b.host.time);
    if (b.genuine && b.genuine.length) {
        $("#logo_img").attr("src", b.genuine);
        $("#logo_info").show()
    } else {
        $("#logo_info").hide()
    }
}
function get_eth_str(b) {
    if (b && b.plugged != 0) {
        if (b.speed > 0) {
            var a = "" + b.speed + "Mbps";
            if (b.duplex == 1) {
                a += "-" + jsTranslate("Full")
            } else {
                if (b.duplex == 0) {
                    a += "-" + jsTranslate("Half")
                }
            }
            return a
        } else {
            return jsTranslate("Plugged")
        }
    } else {
        return jsTranslate("Unplugged")
    }
}
function add_ifinfo(c, a, b) {
    c.push('<div class="row">');
    c.push('<span class="label">' + a + ":</span>");
    c.push('<span class="value">' + b + "</span>");
    c.push("</div>")
}
function update_ifinfo(c) {
    var e = [];
    var g = getIface(c, "ath0");
    if (g) {
        add_ifinfo(e, devname2uidevname(g.ifname) + " MAC", g.hwaddr)
    }
    var f = $.grep(c.interfaces, function(h) {
        return h.ifname.indexOf("eth") == 0
    });
    $.each(f, function(h, j) {
        add_ifinfo(e, devname2uidevname(j.ifname) + " MAC", j.hwaddr)
    });
    var a, d;
    if (f.length == 1) {
        a = devname2uidevname(f[0].ifname);
        d = get_eth_str(f[0].status)
    } else {
        if (f.length > 1) {
            a = devname2uidevname(f[0].ifname) + " / " + devname2uidevname(f[1].ifname);
            d = get_eth_str(f[0].status) + " / " + get_eth_str(f[1].status)
        }
    }
    add_ifinfo(e, a, d);
    var b = e.join("");
    if ($("#ifinfo").html() != b) {
        $("#ifinfo").empty();
        $("#ifinfo").append(b)
    }
}
function update_antenna(a) {
    $("#antenna").text(jsTranslate(a))
}
function update_chains(a) {
    $("#chains").text(a)
}
function update_unms(b) {
    var a = getUnmsStatus(b);
    $("#unms_link").attr("href", a.link);
    $("#unms_status_btn").attr("src", a.img);
    $("#unms_qm").toggleClass("unms_qm", a.qm);
    $("#unms_status").text(a.state)
}
function showAction(a) {
    if (a.value == "") {
        return
    }
    openPage(a.value, 700, 200);
    a.selectedIndex = 0
}
function setExtendedChannel(a) {
    var c = 0;
    var b = 0;
    if (c = /minus$/i.test(a)) {
        $("#ext_chan").html("&nbsp;(" + jsTranslate("Lower") + ")");
        b = -1
    } else {
        if (c = /plus$/i.test(a)) {
            $("#ext_chan").html("&nbsp;(" + jsTranslate("Upper") + ")");
            b = 1
        }
    }
    $("#ext_chan").toggle(c);
    return b
}
function get_gps_quality(b) {
    if (b.fix == 0) {
        return 0
    }
    var c = [[20, 10], [15, 20], [10, 30], [7, 40], [5, 50], [3.5, 60], [2, 70], [1.5, 80], [1, 90], [0, 100]];
    for (var a in c) {
        if (b.dop > c[a][0]) {
            return c[a][1]
        }
    }
    return 0
}
function update_gps(b) {
    $(".gpsinfo").toggle(b.status != 0);
    if (b.status != 0) {
        $("#gps_status").text(jsTranslate("Enabled"));
        var d = get_gps_quality(b);
        $("#gps_qual").text(d);
        update_meter("gpsbar", d, 100);
        var c = (b.fix != 0) ? '<a target="_blank" href=http://maps.google.com/maps?q=' + b.lat + "," + b.lon + ">" + b.lat + " / " + b.lon + "</a>" : "- / -";
        $("#gps_coord").html(c);
        var a = (b.fix != 0) ? "" + Math.round(b.alt) + " m" : "-";
        $("#gps_alt").text(a)
    } else {
        $("#gps_status").text(jsTranslate("Disabled"))
    }
}
function update_airgw(a) {
    if (a != undefined) {
        $("#airgw_info").show()
    } else {
        $("#airgw_info").hide()
    }
}
function isRadarEnabled(a) {
    var c = format_freq(a).split(" ", 1)
      , b = a.channel;
    if (c == "0" || b == 0) {
        return true
    } else {
        return false
    }
}
var autoLogout = function() {
    var b = 15 * 1000;
    var a = 0;
    return {
        start: function(c) {
            if (a) {
                clearTimeout(a)
            }
            if (c) {
                b = c * 1000
            }
            a = setTimeout(function() {
                $.ajax({
                    url: "/logout.cgi",
                    data: {
                        redirect: "false"
                    },
                    cache: false,
                    success: function(e, d) {
                        window.location.reload()
                    }
                })
            }, b)
        },
        stop: function() {
            if (a) {
                clearTimeout(a)
            }
        }
    }
}();
